﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pizza4Dead2
{
    public class DeliveryManager : MonoBehaviour
    {
        [SerializeField] private Transform targetPointer;

        private Transform[] targets;
        private int delivered = 0;
        private int currentTarget = -1;

        private MenuControler menuControler;

        void Start()
        {
            targets = new Transform[transform.childCount];

            for(int i = 0; i < transform.childCount; i++)
                targets[i] = transform.GetChild(i);

            GetNewTarget();

            menuControler = FindObjectOfType<MenuControler>();
        }

        public void Deliver()
        {
            delivered++;
            GetNewTarget();

            menuControler.UpdateDeliveredText(delivered);
        }

        private void GetNewTarget()
        {
            int newTarget = currentTarget;
            while(newTarget == currentTarget)
            {
                newTarget = Random.Range(0, targets.Length - 1);
            }

            currentTarget = newTarget;
            targetPointer.position = targets[currentTarget].position;
        }

        public void GameOver()
        {
            menuControler.GameOver(delivered);
        }
    }

}