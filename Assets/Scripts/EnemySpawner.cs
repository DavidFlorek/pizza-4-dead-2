﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pizza4Dead2
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private float left;
        [SerializeField] private float right;
        [SerializeField] private float top;
        [SerializeField] private float bottom;

        [SerializeField] private float spawnDelay;

        [SerializeField] private GameObject[] prefabs;

        private float currentSpawnDelay;

        void Update()
        {
            currentSpawnDelay -= Time.deltaTime;
            if (currentSpawnDelay < 0)
                Spawn();
        }

        private void Spawn()
        {
            currentSpawnDelay = spawnDelay;
            Vector3 spawnPosition = Vector3.zero;
            int spawnRandom = Random.Range(0, 3);

            switch (spawnRandom)
            {
                case 0: spawnPosition.x = left; break;
                case 1: spawnPosition.x = right; break;
                case 2: spawnPosition.y = top; break;
                case 3: spawnPosition.y = bottom; break;
            }
            Instantiate(prefabs[Random.Range(0, prefabs.Length)], spawnPosition, Quaternion.identity);
        }
    }

}