﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pizza4Dead2
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private float range;
        [SerializeField] private float attackDelay;

        private Vector3 movementVector;
        private bool attacking = false;

        private Transform player;
        private Animator animator;
        private Rigidbody2D rb2d;

        void Start()
        {
            player = FindObjectOfType<PlayerMovement>().transform;
            animator = GetComponent<Animator>();
            rb2d = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if (!attacking)
            {
                if (Vector2.Distance(transform.position, player.position) < range)
                {
                    movementVector = Vector3.zero;
                    StartCoroutine(Attack());
                }
                else
                {
                    movementVector.x = player.position.x - transform.position.x;
                    movementVector.y = player.position.y - transform.position.y;
                    movementVector.Normalize();
                }
            }

            animator.SetFloat("Speed", movementVector.sqrMagnitude);
        }

        private void FixedUpdate()
        {
            if (movementVector.magnitude > 0)
            {
                rb2d.velocity = movementVector * speed * Time.deltaTime;

                float angle = Mathf.Atan2(-movementVector.x, movementVector.y) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            } else
            {
                rb2d.velocity = Vector2.zero;
            }
        }

        private IEnumerator Attack()
        {
            attacking = true;

            yield return new WaitForSeconds(attackDelay);

            animator.SetTrigger("Attack");

            if (Vector2.Distance(transform.position, player.position) < range)
            {
                // Game over
                FindObjectOfType<DeliveryManager>().GameOver();
            }

            attacking = false;
        }
    }
}
