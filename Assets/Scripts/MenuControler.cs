﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pizza4Dead2
{
    public class MenuControler : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenuScreen;
        [SerializeField] private GameObject inGameScreen;
        [SerializeField] private GameObject gameOverScreen;
        [SerializeField] private GameObject storyScreen;

        [SerializeField] private Text gameOverText;

        private Text scoreText;

        void Start()
        {
            DontDestroyOnLoad(gameObject);

            scoreText = inGameScreen.GetComponentInChildren<Text>();

            SceneManager.LoadScene(1);
        }

        public void Exit()
        {
            Application.Quit();
        }

        public void ShowStory()
        {
            mainMenuScreen.SetActive(false);
            inGameScreen.SetActive(false);
            gameOverScreen.SetActive(false);
            storyScreen.SetActive(true);
        }

        public void Play()
        {
            mainMenuScreen.SetActive(false);
            inGameScreen.SetActive(true);
            gameOverScreen.SetActive(false);
            storyScreen.SetActive(false);

            SceneManager.LoadScene(2);
        }

        public void GameOver(int delivered)
        {
            mainMenuScreen.SetActive(false);
            inGameScreen.SetActive(false);
            gameOverScreen.SetActive(true);
            storyScreen.SetActive(false);

            gameOverText.text = string.Format("Game over!\nYou delivered {0} pizzas!", delivered);

            SceneManager.LoadScene(1);
        }

        public void MainMenu()
        {
            mainMenuScreen.SetActive(true);
            inGameScreen.SetActive(false);
            gameOverScreen.SetActive(false);
            storyScreen.SetActive(false);
        }

        public void UpdateDeliveredText(int delivered)
        {
            scoreText.text = string.Format("DELIVERED: {0}", delivered);
        }
    }

}