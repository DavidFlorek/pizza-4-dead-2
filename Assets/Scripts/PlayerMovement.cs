﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pizza4Dead2
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float speed;

        private float verticalInput;
        private float horizontalInput;
        private float animationSpeed;
        private Vector3 inputVector;

        private DeliveryManager deliveryManager;
        private Animator animator;
        private Rigidbody2D rb2d;
        private AudioSource audio;

        private void Start()
        {
            deliveryManager = FindObjectOfType<DeliveryManager>();
            animator = GetComponent<Animator>();
            rb2d = GetComponent<Rigidbody2D>();
            audio = GetComponent<AudioSource>();
        }

        void Update()
        {
            verticalInput = Input.GetAxisRaw("Vertical");
            horizontalInput = Input.GetAxisRaw("Horizontal");

            inputVector = new Vector3(horizontalInput, verticalInput, 0).normalized;

            animationSpeed = inputVector.sqrMagnitude;

            animator.SetFloat("Speed", animationSpeed);
        }

        private void FixedUpdate()
        {
            if (inputVector.magnitude > 0)
            {
                rb2d.velocity = inputVector * speed * Time.deltaTime;

                float angle = Mathf.Atan2(-inputVector.x, inputVector.y) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            } else
            {
                rb2d.velocity = Vector2.zero;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Target"))
            {
                deliveryManager.Deliver();
                audio.Play();
            }
        }
    }

}